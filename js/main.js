/*Завдання
Написати реалізацію кнопки "Показати пароль". Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

Технічні вимоги:
У файлі index.html лежить розмітка двох полів вводу пароля.
Після натискання на іконку поруч із конкретним полем - повинні відображатися символи, які ввів користувач, іконка змінює свій зовнішній вигляд. У коментарях під іконкою - інша іконка, саме вона повинна відображатися замість поточної.
Коли пароля не видно - іконка поля має виглядати як та, що в першому полі (Ввести пароль)
Коли натиснута іконка, вона має виглядати, як та, що у другому полі (Ввести пароль)
Натиснувши кнопку Підтвердити, потрібно порівняти введені значення в полях
Якщо значення збігаються – вивести модальне вікно (можна alert) з текстом – You are welcome;
Якщо значення не збігаються - вивести під другим полем текст червоного кольору Потрібно ввести однакові значення
Після натискання на кнопку сторінка не повинна перезавантажуватись
Можна міняти розмітку, додавати атрибути, теги, id, класи тощо.*/

let eyes = document.querySelectorAll(".fas");
eyes.forEach((eye) => {
  eye.addEventListener("click", () => {
    eye.classList.toggle("fa-eye-slash");
    let input = eye.previousElementSibling;
    if (input.type === "password") {
      input.type = "text";
    } else {
      input.type = "password";
    }
  });
});

let form = document.querySelector(".password-form");
let password = document.getElementById("first");
let confirm = document.getElementById("confirm");
let message = document.createElement("p");
message.innerText = "Потрібно ввести однакові значення";
let btn = document.querySelector(".btn");
btn.before(message);
message.style.cssText =
  "color : red; margin : 5px; font-size: 20px; display : none";
form.addEventListener("submit", (event) => {
  if (password.value === confirm.value && password.value !== "") {
    alert("You are welcome");
    message.style.display = "none";
  } else message.style.display = "block";
  confirm.style.marginBottom = "3px";
  event.preventDefault();
});
